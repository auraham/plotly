c









- [ ] agregar highlight by name
- [ ] agregar boton de reset selection
- [ ] falta hacer un reset cuando se hace clic en un punto por segunda vez



- [ ] reemplazar esto

```
let trace_a = {
    x: unpack(rows, 'x'),
    y: unpack(rows, 'a'),
    name: 'Serie A',
    type: 'scatter',
    mode: 'lines+markers',
    marker: {
        color: 'rgb(243, 186, 91)',
        size: 10,
        symbol: 'circle',
        opacity: 0.8,
        line: {
            color: 'rgb(120, 120, 120)',
            width: 1
        }
    },
    line: {
        color: 'rgb(243, 186, 91)',
        width: 2
    }
};

```

por esto

```
let trace_a = createTrace(rows, name, color, x = 'x', y='a');
```

para evitar trace mutables (bueno, mas bien para reducir la mutacion entre traces)

- [ ] Crear funcion para definir estilo por defecto





















- [ ] determinar como compartir axes (al final)
- [ ] compartir axes en un scattermatrix (al final)
- [ ] resaltar el mismo punto en
  - [ ] scatter matrix
  - [ ] pcp  (usar solo una serie, quitar el trace de los knee points, ahora mejor solo los resaltaremos al hacer clic o con un boton)
    - [ ] agregar manejo de evento click sobre un punto y actualizar otro plot
  - [ ] radviz



- [x] agregar plot.on(event) en scatter
- [x] agregar seleccion lasso en scatter
- [ ] agregar `scatter_matrix_pcp.html`
- [ ] hacer un fetch de un csv





- [ ] crear un scatter matrix con un numero variable de rows y cols
  - [ ] definir los axes automaticamente
- [ ] crear scatter row con plots definidos por el usuario

- [x] integrar el codigo de paco
- [ ] enviar paper corregido
- [ ] agregar app con web2py
- [ ] pasar mis vistas a web2py
- [ ] descargar el theme piaf
- [ ] integrar theme en web2py
- [ ] contactar a zapotecas
- [ ] actualizar documentos para segundo proyecto
- [ ] descargar musica first suits



- [x] continua actualizando el estilo del pcp a parte



```

```







- [x] trabajar en el pcp
- [x] integrar scatter y pcp 
  - [x] resaltar dos puntos con un button
  - [x] resaltar puntos al hacer clic en el scatter
  - [ ] hacer demo
    - [ ] refactorizar codigo en demo.js
- [ ] Crear las siguientes funciones (organizar codigo)

```javascript
function unpack(rows, key) {}

function makeTrace2DScatter(name, rows, xdata, ydata, xaxis, yaxis, color) {}
function makeTracePCP(name, rows, xdata, ydata, xaxis, yaxis, color) {}


function highlightPointsScatter(plotName, pointIDs, traceName, color) {}
function highlightPointsPCP(plotName, pointIDs) {}


// --- better api ---


// UTILS


function scatter_makeTrace2D(name, rows, xdata, ydata, xaxis, yaxis, color) {}
// copia de highlightMultiplePointsByName2()
function scatter_highlightPoints(plotName, pointIDs, traceName, color) {} 
function scatter_copyMarker() {}
// copia de getAllTraceIdByName()
function scatter_getTraceIDsByName()

function pcp_makeTrace(name, rows, xdata, ydata, xaxis, yaxis, color) {}
// copia de highlightMultilePointsPCP()
function pcp_highlightPoints(plotName, pointIDs) {}

// -- others --


```

- [x] corregir punto negro



## Utils

| Old function        | New function        |
| ------------------- | ------------------- |
| `unpack(rows, key)` | `unpack(rows, key)` |



## Scatter matrix

| Old function                                                 | New function                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `makeTrace2D(name, rows, xdata, ydata, xaxis, yaxis, markerColor)` | `scatter_makeTrace2D(name, rows, xdata, ydata, xaxis, yaxis, color)` |
| `getAllTraceIdByName(plotName, traceName)`                   | `scatter_getTraceIDsByName(plotName, traceName)`             |
| `copyMarker(marker)`                                         | `scatter_copyMarker(marker)`                                 |
| `function highlightPointByName2(plotName, traceName, pointID, new_color="red")` | deprecated                                                   |
| `highlightMultiplePointsByName2(plotName, traceName, pointIDs, new_color="red")` | `scatter_highlightPoints(plotName, pointIDs, traceName, color)` |



## PCP

| Old function                                    | New function                                                 |
| ----------------------------------------------- | ------------------------------------------------------------ |
| `highlightMultilePointsPCP(plotName, pointIDs)` | `pcp_highlightPoints(plotName, pointIDs)`                    |
| does not exist                                  | `pcp_makeTrace(traceName, rows, xdata, ydata, xaxis, yaxis, markerColor)` |
|                                                 |                                                              |
|                                                 |                                                              |
|                                                 |                                                              |
|                                                 |                                                              |
|                                                 |                                                              |
|                                                 |                                                              |
|                                                 |                                                              |



- [x] checar los precios de mi cotizacion con los que me mando heydi
- [x] enviar los datos a neyva
- [ ] armar documentacion proyecto dos
- [ ] pedir cotizacion a alguien mas
- [ ] hacer correcciones para meter el paper de open access
- [ ] checar curso gratis coursera
- [ ] checar un framework de documentacion