## First example



**Controller**

```python
# controllers/default.py
def index():        # the functions in the controller are called actions
    return dict(message="Hello from myapp")
```

**View**

```html
# views/default/index.html
<html>
  <head>
  </head>
  <body>
    <h1>{{=message}}</h1>
  </body>
</html>
```

When an action returns a dictionary, web2py looks for a view with the name:

```
[controller]/[function].[extension]
```

and executes it. Here `[extension]` is the requested extension. If no extension is specified, it defaults to `html`, and that is what we will assume here. Under this assumption, the view is an HTML file that embeds Python code using special `{{ }}` tags. If web2py does not find the requested view, it uses the `generic.html` view that comes with every application.

You can specify a view for other file types, like `view.json`, `view.xml`, and so on. Check Chapter 10. You can also specify a view with:

```python
response.view = 'default/something.html'
```

Add this to the view to show debugging information:

```
{{=response.toolbar()}}
```

It uses jQuery, so be sure to include it in the view.



## Add a counter

**Controller**

```python
# controllers/default.py
def index():
    if not session.counter:
        session.counter = 1
    else:
        session.counter += 1
    return dict(message="Hello from myapp", counter=session.counter)
```

**View**

```html
# views/default/index.html
<html>
  <head>
  </head>
  <body>
    <h1>{{=message}}</h1>
    <h2>Number of visits: {{=counter}}</h2>
    {{=response.toolbar()}}
  </body>
</html>
```



## Requests

**Controller**

```python
# controllers/default.py
def first():
    return dict()

def second():
    return dict()
```

**Views**

`default/first.html`

```jinja2
{{extend 'layout.html'}}
<h1>What is your name?</h1>
<form action="{{=URL('second')}}">
    <input type="text" name="visitor_name" />
    <input type="submit" />
</form>
```

`default/second.html`

```jinja2
{{extend 'layout.html'}}
<h1>Hello {{=request.vars.visitor_name}}</h1>
{{=response.toolbar()}}
```



## Post backs

`controllers/default.py`

```python
def first():
    if request.vars.visitor_name:
        # guarda el nombre en la sesion
        session.visitor_name = request.vars.visitor_name
        redirect(URL('second'))
    return dict()

def second():
    return dict()
```

**Views**

`default/first.html`

```html
{{extend 'layout.html'}}
<h1>What is your name?</h1>
<form>
    <input type="text" name="visitor_name" />
    <input type="submit" />
</form>
```

`default/second.html`

```html
{{extend 'layout.html'}}
<h1>Hello {{=session.visitor_name or "anonymous"}}</h1>
{{=response.toolbar()}}
```

----

**Note** I suppose that `session` and `request` are objects, since we can access attributes with `session.visitor_name`. If the attribute does not exists, its value is `None` since we can evaluate expressions like `if request.vars.visitor_name:` or `session.visitor_name or "anonymous"`. I need to verify this.

----



## Better forms



**Controllers**

`controllers/default.py`

```python
def first():
    form = SQLFORM.factory(Field("visitor_name",
                                 label="What is your name?",
                                 requires=IS_NOT_EMPTY()
                                ))
    if form.process().accepted:
        session.visitor_name = form.vars.visitor_name
        redirect(URL('second'))
    
    return dict(form=form)
```

The `form.process()` method applies the validators and returns the form itself. The `form.accepted` variable is set to True if the form was processed and passed validation. If the self-submitted form passes validation, it stores the variables in the session and redirects as before. If the form does not pass validation, error messages are inserted into the form

**Views**

`views/default/first.html`

```
{{extend 'layout.html'}}
{{=form}}
```









## form, request

From [this post](http://web2py.com/books/default/chapter/29/07/forms-and-validators#SQLFORM),

> Both `form.vars` and `form.errors` are `gluon.storage.Storage` objects similar to `request.vars`.



```python
def display_form():
    form = FORM('Your name:',
                INPUT(_name='name', requires=IS_NOT_EMPTY()),
                INPUT(_type='submit'))

    if form.accepts(request, session):      # all the work is done here
        response.flash = "form accepted"

        # debug
        # form.vars <Storage {'name': 'aura'}>
        print("form.vars", form.vars)

    elif form.errors:
        response.flash = "form has errors"

        # debug
        # form.errors <Storage {'name': 'Enter a value'}>
        print("form.errors", form.errors)

    else:
        response.flash = "please fill the form"
        
        # debug
        # response.flash please fill the form
        print("response.flash", response.flash)


    return dict(form=form)
```

```
form.vars <Storage {'name': 'aura'}>
form.errors <Storage {'name': 'Enter a value'}>
response.flash please fill the form
request.vars <Storage {'_formname': 'no_table/create', '_formkey': '821566b5-fe21-4a99-932e-d00ef3f6634a', 'courses': 'a'}>
```



## form.process() vs form.accepts()

This

```
form.accepts(request.post_vars, session, ...)
```

is a shortcut for this:

```
form.process(...).accepted
```





## Extend and include views

`views/dashboard/test.html`

```html
{{extend 'layout_test.html'}}

{{block mysidebar}}
    over!
{{end}}

test.html
```

`views/layout_test.html`

```html
<!DOCTYPE html>
  <head>
    <title>{{=response.title or request.application}}</title>
  </head>
  <body>
    
    This is the body
          
    <hr>

    <div class="container-fluid main-container">
      {{include}}
    </div>

    <hr>

    <div class="sidebar">
        {{block mysidebar}}
            this is my default sidebar
        {{end}}
    </div>

    This is the bottom

  </body>
</html>
```

This is the result:

![](views.bmp)

- The content of `test.html` is embedded in the `{{include}}` of `layout_test.html`.
- `layout_test.html` contains a block, `{{block mysidebar}}` with some default content.
- If the view `test.html` does not contains a `	{{block mysidebar}}`, then we use the default content given in `layout_test.html`. In this case, we override it.





## Export json

This is the magic:

```python
# gluon/globals.py
from gluon.serializers import json, custom_json

def json(self, data, default=None, indent=None):
    if 'Content-Type' not in self.headers:
        self.headers['Content-Type'] = 'application/json'
    return json(data, default=default or custom_json, indent=indent)
```

```python
# serializers.py

import json as json_parser

def json(value, default=custom_json, indent=None, sort_keys=True, cls=JSONEncoderForHTML):
    return json_parser.dumps(value, default=default, cls=cls, sort_keys=sort_keys, indent=indent)

```

It uses `json`, a standard python package. 

**Note** I modified the code:`sort_keys=True`.