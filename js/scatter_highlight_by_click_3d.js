/* scatter_highlight_by_click_3d.js */
// @ts-check

// --- utils ---

function unpack(rows, key) {
    return rows.map(row => row[key]);
}

// let trace_a = createTrace(rows, name, color, x = 'x', y='a');
/*
 Creates a new trace
*/
function makeTrace2D(name, rows, xdata, ydata, markerColor) {
    return {
        x: unpack(rows, xdata),
        y: unpack(rows, ydata),
        name: name,
        type: 'scatter',
        mode: 'lines+markers',
        marker: {
            color: markerColor,
            size: 10,
            symbol: 'circle',
            opacity: 0.8,
            line: {
                color: 'rgb(120, 120, 120)',
                width: 1
            }
        },
        line: {
            color: markerColor,
            width: 2
        }
    }
}

function makeTrace3D(name, rows, xdata, ydata, zdata, markerColor) {
    return {
        x: unpack(rows, xdata),
        y: unpack(rows, ydata),
        z: unpack(rows, zdata),
        name: name,
        type: 'scatter3d',
        mode: 'markers',
        marker: {
            color: markerColor,
            size: 10,
            symbol: 'circle',
            opacity: 0.8,
            line: {
                color: 'rgb(120, 120, 120)',
                width: 1
            }
        }
    }
}

// --- get data ---

let rows = [
    {x: 0, a: 0, b: 1, c: 2},
    {x: 1, a: 1, b: 2, c: 3},
    {x: 2, a: 2, b: 3, c: 4},
    {x: 3, a: 3, b: 4, c: 5},
];

// --- plot ---

let layout = {
    width: 600,
    height: 400,
    hovermode: 'closest'
};

let trace_2D_a1 = makeTrace2D('Serie A', rows,  'x', 'a', 'rgb(243, 186, 91)');
let trace_2D_a2 = makeTrace2D('Serie A', rows,  'x', 'a', 'rgb(243, 186, 91)');
let trace_2D_b  = makeTrace2D('Serie B', rows,  'x', 'b', 'rgb(139, 167, 177)');
let trace_2D_c  = makeTrace2D('Serie C', rows,  'x', 'c', 'rgb(139, 177, 161)');

let trace_3D_a1 = makeTrace3D('Serie A', rows,  'a', 'b', 'c', 'rgb(243, 186, 91)');


Plotly.plot('scatter_a', [trace_2D_a1, trace_2D_b], layout);
Plotly.plot('scatter_b', [trace_2D_a2, trace_2D_c], layout);
Plotly.plot('scatter_c', [trace_3D_a1], layout);


// --- add event handler ----




// ----

// debug
let scatter_a = document.getElementById('scatter_a');

// show points
let trace_id = 0;
console.log(scatter_a.data[trace_id].x);  // componente x
console.log(scatter_a.data[trace_id].y);  // componente y



// --- highlight by name ---

/*
 Returns the map corresponding of the trace called 'traceName' in plotName.
 If the trace is not found, it returns null.
*/
function getTraceByName(plotName, traceName) {
    let plot = document.getElementById(plotName);
    let traces = plot.data.filter(trace => trace['name'] == traceName);

    if (traces.length == 1) {
        return traces[0];
    }

    return null;
}

/*
 Returns the id of the trace called traceName in plotName.
*/
function getTraceIdByName(plotName, traceName) {

    let id = null;

    let plot = document.getElementById(plotName);
    
    // check if plot is valid
    if (plot === null) {
        console.warn(`Invalid plotName (plotName: ${plotName}, traceName: ${traceName})`);
        return id;
    } 
    
    let traces = plot.data;
    for (let i=0; i<traces.length; i++) {
        if (traces[i].name == traceName) {
            id = i;
            break;
        }
    }
    
    return id;
}

let plotName = 'scatter_a';
let traceName = 'Serie A';
let pointID = 3;
let plot = document.getElementById(plotName);


// ----- add event handler -------



// https://plotly.com/javascript/click-events/
// https://codepen.io/plotly/pen/QbZmZY?editors=1010
plot.on('plotly_click', function(data){
    let pts = '';
    for (let i=0; i<data.points.length; i++) {
        pts = `x = ${data.points[i].x}, y = ${data.points[i].y.toPrecision(4)}`;
    }
    //alert('Closest point clicked' + pts);
    //console.log(pts);

    // debug
    //debugger;

    // get more data
    let traceName = data.points[0].data.name; // asumiendo que solo hay un punto en points (ie que se hizo click en un solo punto)
    let pointID = data.points[0].pointNumber;

    console.log(`traceName: ${traceName}, pointID: ${pointID}`);

    debugger;

    // highlight the selected point in other plots
    highlightPointByName("scatter_a", traceName, pointID, "red");

    debugger;

    highlightPointByName("scatter_b", traceName, pointID, "red");
    
    debugger;

    highlightPointByName("scatter_c", traceName, pointID, "red");
});


let plot_b = document.getElementById('scatter_b');

plot_b.on('plotly_click', function(data){
    let pts = '';
    for (let i=0; i<data.points.length; i++) {
        pts = `x = ${data.points[i].x}, y = ${data.points[i].y.toPrecision(4)}`;
    }

    // get more data
    let traceName = data.points[0].data.name; // asumiendo que solo hay un punto en points (ie que se hizo click en un solo punto)
    let pointID = data.points[0].pointNumber;

    console.log(`traceName: ${traceName}, pointID: ${pointID}`);

    debugger;

    // highlight the selected point in other plots
    highlightPointByName("scatter_a", traceName, pointID, "red");

    debugger;

    highlightPointByName("scatter_b", traceName, pointID, "red");
    
    debugger;

    highlightPointByName("scatter_c", traceName, pointID, "red");
});

let plot_c = document.getElementById('scatter_c');

plot_c.on('plotly_click', function(data){
    let pts = '';
    for (let i=0; i<data.points.length; i++) {
        pts = `x = ${data.points[i].x}, y = ${data.points[i].y.toPrecision(4)}`;
    }

    // get more data
    let traceName = data.points[0].data.name; // asumiendo que solo hay un punto en points (ie que se hizo click en un solo punto)
    let pointID = data.points[0].pointNumber;

    console.log(`traceName: ${traceName}, pointID: ${pointID}`);

    debugger;

    // highlight the selected point in other plots
    highlightPointByName("scatter_a", traceName, pointID, "red");

    debugger;

    highlightPointByName("scatter_b", traceName, pointID, "red");
    
    debugger;

    highlightPointByName("scatter_c", traceName, pointID, "red");
});



// --- utils ---

function highlightPoint(plotName, traceID, pointID) {

    let plot = document.getElementById(plotName);

    // prev style
    let marker = plot.data[traceID].marker;
    let default_color = marker['color'];

    // aqui hay una condicion
    // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
    // entonces todo el trace es de ese color
    // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
    // entonces, cada punto del trace tiene un color asignado
    // debemos manejar ambos casos

    debugger;

    let colors;
    if (typeof(marker['color']) == 'string') {
        // el mismo color para todos
        colors = plot.data[traceID].x.map(item => default_color);
    }
    else {
        // un color para cada punto
        colors = marker['color'].map(item => item);
    }

    colors[pointID] = "red";                                  // hightlight point

    // update style
    marker['color'] = colors; // no ocupa wrapper
    let update = {
        marker: marker
    };

    Plotly.restyle(plotName, update, [traceID]);
    //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces
}




function copyMarker(marker) {

    let marker_color;
    if (typeof(marker['color']) == 'string') {
        marker_color = marker['color'];                     // copy string
    }
    else {
        marker_color = marker['color'].map(item => item);   // copy list
    }
    
    let line_color;
    if (typeof(marker.line['color']) == 'string') {
        line_color = marker.line['color'];                     // copy string
    }
    else {
        debugger;
        line_color = marker.line['color'].map(item => item);   // copy list
    }



    let newMarker = {
        color: marker_color,
        size: marker['size'],
        symbol: marker['symbol'],
        opacity: marker['opacity'],
        line: {
            color: line_color,
            width: marker.line['width']
        }
    };

    return newMarker;
}

/*
 Highlight a point with ID pointID in the trace called traceName in plotName
*/
function highlightPointByName(plotName, traceName, pointID, new_color="red") {

    let plot = document.getElementById(plotName);
    let traceID = getTraceIdByName(plotName, traceName);

    // debug
    //debugger;

    // check if references are valid
    if (plot === null || traceID === null) {
        console.warn(`Invalid traceName (plotName: ${plotName}, traceName: ${traceName}, pointID: ${pointID})`);
        return 0;
    }

    // prev style
    ///let marker = plot.data[traceID].marker; // esta linea es mutable, mejor crear un marker con un estilo por defecto
    let marker = copyMarker(plot.data[traceID].marker);  // nuevo
    let default_color = marker['color'];

    // aqui hay una condicion
    // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
    // entonces todo el trace es de ese color
    // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
    // entonces, cada punto del trace tiene un color asignado
    // debemos manejar ambos casos

    //debugger;

    let colors;
    if (typeof(marker['color']) == 'string') {
        // el mismo color para todos
        colors = plot.data[traceID].x.map(item => default_color);
    }
    else {
        // un color para cada punto
        colors = marker['color'].map(item => item);
    }

    // original color of trace[pointID]
    let base_color;
    for (let j=0; j<colors.length; j++) {
        if (j != pointID && colors[j] != new_color) {
            base_color = colors[j];
            break;
        }
    }

    if (colors[pointID] == new_color) {
        // go back to the original color
        colors[pointID] = base_color;
    }
    else {
        // change its color
        colors[pointID] = new_color;
    }
    
    //colors[pointID] = "red";                                  // hightlight point
    //colors[pointID] = new_color;                                  // hightlight point

    // update style
    marker['color'] = colors; // no ocupa wrapper
    let update = {
        marker: marker
    };

    //Plotly.restyle(plotName, update, [traceID]);
    //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces

    // we need to set a timeout to avoid a recursive call
    // when the click is done in a scatter3D
    // https://github.com/plotly/plotly.js/issues/1025
    setTimeout(() => {
        Plotly.restyle(plotName, update, [traceID]);
    }, 200);
}