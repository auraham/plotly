/* scatter_matrix.js */
// @ts-check

function unpack(rows, key) {
    return rows.map(row => row[key]);
}

/*
 Creates a new trace
*/
function makeTrace2D(name, rows, xdata, ydata, xaxis, yaxis, markerColor) {
    return {
        x: unpack(rows, xdata),
        y: unpack(rows, ydata),
        name: name,
        type: 'scatter',
        mode: 'markers',
        xaxis: xaxis,
        yaxis: yaxis,
        marker: {
            color: markerColor,
            size: 10,
            symbol: 'circle',
            opacity: 0.8,
            line: {
                color: 'rgb(120, 120, 120)',
                width: 1
            }
        }
    };
}

// --- get data ---

let rows = [
    {a: 0, b: 1, c: 2},
    {a: 1, b: 2, c: 3},
    {a: 2, b: 3, c: 4},
    {a: 3, b: 4, c: 5},
];

// --- plot ---

let layout = {
    dragmode: 'lasso',
    width: 900,
    height: 900,
    grid: {rows: 3, columns: 3, pattern: 'independent'},
    showlegend: false,
    
    xaxis:  { title: { text: "a x1" }, range: [0, 70]},
    xaxis2: { title: { text: "b x2" }, range: [0, 70]},
    xaxis3: { title: { text: "c x3" }, range: [0, 70]},
    xaxis4: { title: { text: "a x4" }, range: [0, 70]},
    xaxis5: { title: { text: "b x5" }, range: [0, 70]},
    xaxis6: { title: { text: "c x6" }, range: [0, 70]},
    xaxis7: { title: { text: "a x7" }, range: [0, 70]},
    xaxis8: { title: { text: "b x8" }, range: [0, 70]},
    xaxis9: { title: { text: "c x9" }, range: [0, 70]},

    yaxis:  { title: { text: "a y1" }, range: [0, 70]},
    yaxis2: { title: { text: "a y2" }, range: [0, 70]},
    yaxis3: { title: { text: "a y3" }, range: [0, 70]},
    yaxis4: { title: { text: "b y4" }, range: [0, 70]},
    yaxis5: { title: { text: "b y5" }, range: [0, 70]},
    yaxis6: { title: { text: "b y6" }, range: [0, 70]},
    yaxis7: { title: { text: "c y7" }, range: [0, 70]},
    yaxis8: { title: { text: "c y8" }, range: [0, 70]},
    yaxis9: { title: { text: "c y9" }, range: [0, 70]},
    
    //hovermode: 'closest'
};

let trace_aa = makeTrace2D('Serie', rows, 'a', 'a', 'x1', 'y1', 'rgb(243, 186, 91)');
let trace_ba = makeTrace2D('Serie', rows, 'b', 'a', 'x2', 'y2', 'red');
let trace_ca = makeTrace2D('Serie', rows, 'c', 'a', 'x3', 'y3', 'rgb(243, 186, 91)');

let trace_ab = makeTrace2D('Serie', rows, 'a', 'b', 'x4', 'y4', 'rgb(243, 186, 91)');
let trace_bb = makeTrace2D('Serie', rows, 'b', 'b', 'x5', 'y5', 'rgb(243, 186, 91)');
let trace_cb = makeTrace2D('Serie', rows, 'c', 'b', 'x6', 'y6', 'rgb(243, 186, 91)');

let trace_ac = makeTrace2D('Serie', rows, 'a', 'c', 'x7', 'y7', 'rgb(243, 186, 91)');
let trace_bc = makeTrace2D('Serie', rows, 'b', 'c', 'x8', 'y8', 'rgb(243, 186, 91)');
let trace_cc = makeTrace2D('Serie', rows, 'c', 'c', 'x9', 'y9', 'rgb(243, 186, 91)');

Plotly.plot('scatter_matrix', [
        trace_aa, trace_ab, trace_ac,
        trace_ba, trace_bb, trace_bc,
        trace_ca, trace_cb, trace_cc,
    ], layout);

let plt = document.getElementById('scatter_matrix');

//debugger;
// -----------------------------------

/* scatter_highlight_by_click_3d.js */
// @ts-check

// --- utils ---

function unpack(rows, key) {
    return rows.map(row => row[key]);
}

/*
 Returns the id of the trace called traceName in plotName.
*/
function getAllTraceIdByName(plotName, traceName) {

    let ids = [];

    let plot = document.getElementById(plotName);
    
    // check if plot is valid
    if (plot === null) {
        console.warn(`Invalid plotName (plotName: ${plotName}, traceName: ${traceName})`);
        return ids;
    }

    
    let traces = plot.data;
    for (let i=0; i<traces.length; i++) {
        if (traces[i].name == traceName) {
            ids.push(i);
        }
    }
    
    return ids;
}

function copyMarker(marker) {

    let marker_color;
    if (typeof(marker['color']) == 'string') {
        marker_color = marker['color'];                     // copy string
    }
    else {
        marker_color = marker['color'].map(item => item);   // copy list
    }
    
    let line_color;
    if (typeof(marker.line['color']) == 'string') {
        line_color = marker.line['color'];                     // copy string
    }
    else {
        debugger;
        line_color = marker.line['color'].map(item => item);   // copy list
    }



    let newMarker = {
        color: marker_color,
        size: marker['size'],
        symbol: marker['symbol'],
        opacity: marker['opacity'],
        line: {
            color: line_color,
            width: marker.line['width']
        }
    };

    return newMarker;
}

/*
 Highlight a point with ID pointID in the trace called traceName in plotName
*/
function highlightPointByName(plotName, traceName, pointID, new_color="red") {

    debugger;

    let plot = document.getElementById(plotName);
    let traceIDs = getAllTraceIdByName(plotName, traceName);

    // debug
    //debugger;

    // check if references are valid
    if (plot === null || traceIDs === null) {
        console.warn(`Invalid traceName (plotName: ${plotName}, traceName: ${traceName}, pointID: ${pointID})`);
        return 0;
    }

    for (let traceID of traceIDs) {

        // prev style
        ///let marker = plot.data[traceID].marker; // esta linea es mutable, mejor crear un marker con un estilo por defecto
        let marker = copyMarker(plot.data[traceID].marker);  // nuevo
        let default_color = marker['color'];

        // aqui hay una condicion
        // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
        // entonces todo el trace es de ese color
        // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
        // entonces, cada punto del trace tiene un color asignado
        // debemos manejar ambos casos

        //debugger;

        let colors;
        if (typeof(marker['color']) == 'string') {
            // el mismo color para todos
            colors = plot.data[traceID].x.map(item => default_color);
        }
        else {
            // un color para cada punto
            colors = marker['color'].map(item => item);
        }

        // original color of trace[pointID]
        let base_color;
        for (let j=0; j<colors.length; j++) {
            if (j != pointID && colors[j] != new_color) {
                base_color = colors[j];
                break;
            }
        }

        if (colors[pointID] == new_color) {
            // go back to the original color
            colors[pointID] = base_color;
        }
        else {
            // change its color
            colors[pointID] = new_color;
        }
        
        //colors[pointID] = "red";                                  // hightlight point
        //colors[pointID] = new_color;                                  // hightlight point

        // update style
        marker['color'] = colors; // no ocupa wrapper
        let update = {
            marker: marker
        };

        Plotly.restyle(plotName, update, [traceID]);
        //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces

        // we need to set a timeout to avoid a recursive call
        // when the click is done in a scatter3D
        // https://github.com/plotly/plotly.js/issues/1025
        //setTimeout(() => {
        //    Plotly.restyle(plotName, update, [traceID]);
        //}, 200);

        // debug
        console.log(`Update plot: ${plotName}, traceID: ${traceID}`);
    }
}

// Similar a highlightPointByName
// highlightPointByName:  Usa Plotly.restyle(plotName, update, [traceID]);
// highlightPointByName2: Usa Plotly.restyle(plotName, update, traceIDs);
function highlightPointByName2(plotName, traceName, pointID, new_color="red") {

    let plot = document.getElementById(plotName);
    let traceIDs = getAllTraceIdByName(plotName, traceName);

    // debug
    //debugger;

    // check if references are valid
    if (plot === null || traceIDs === null) {
        console.warn(`Invalid traceName (plotName: ${plotName}, traceName: ${traceName}, pointID: ${pointID})`);
        return 0;
    }

        // trick: a single traceID
        let traceID = traceIDs[0];

        // prev style
        ///let marker = plot.data[traceID].marker; // esta linea es mutable, mejor crear un marker con un estilo por defecto
        let marker = copyMarker(plot.data[traceID].marker);  // nuevo
        let default_color = marker['color'];

        // aqui hay una condicion
        // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
        // entonces todo el trace es de ese color
        // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
        // entonces, cada punto del trace tiene un color asignado
        // debemos manejar ambos casos

        //debugger;

        let colors;
        if (typeof(marker['color']) == 'string') {
            // el mismo color para todos
            colors = plot.data[traceID].x.map(item => default_color);
        }
        else {
            // un color para cada punto
            colors = marker['color'].map(item => item);
        }

        // original color of trace[pointID]
        let base_color;
        for (let j=0; j<colors.length; j++) {
            if (j != pointID && colors[j] != new_color) {
                base_color = colors[j];
                break;
            }
        }

        if (colors[pointID] == new_color) {
            // go back to the original color
            colors[pointID] = base_color;
        }
        else {
            // change its color
            colors[pointID] = new_color;
        }
        
        //colors[pointID] = "red";                                  // hightlight point
        //colors[pointID] = new_color;                                  // hightlight point

        // update style
        marker['color'] = colors; // no ocupa wrapper
        let update = {
            marker: marker
        };

        Plotly.restyle(plotName, update, traceIDs);
        //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces

        // we need to set a timeout to avoid a recursive call
        // when the click is done in a scatter3D
        // https://github.com/plotly/plotly.js/issues/1025
        //setTimeout(() => {
        //    Plotly.restyle(plotName, update, [traceID]);
        //}, 200);

        // debug
        console.log(`Update plot: ${plotName}, traceIDs: ${traceID}`);
    
}

// Similar a highlightMultiplePointByName2 
// highlightPointByName2: pointID es un int 
// highlightMultiplePointByName2: pointID es una lista 
function highlightMultiplePointsByName2(plotName, traceName, pointIDs, new_color="red") {

    let plot = document.getElementById(plotName);
    let traceIDs = getAllTraceIdByName(plotName, traceName);

    // debug
    //debugger;

    // check if references are valid
    if (plot === null || traceIDs === null) {
        console.warn(`Invalid traceName (plotName: ${plotName}, traceName: ${traceName}, pointID: ${pointID})`);
        return 0;
    }

        // trick: a single traceID
        let traceID = traceIDs[0];

        // prev style
        ///let marker = plot.data[traceID].marker; // esta linea es mutable, mejor crear un marker con un estilo por defecto
        let marker = copyMarker(plot.data[traceID].marker);  // nuevo
        let default_color = marker['color'];

        // aqui hay una condicion
        // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
        // entonces todo el trace es de ese color
        // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
        // entonces, cada punto del trace tiene un color asignado
        // debemos manejar ambos casos

        //debugger;

        let colors;
        if (typeof(marker['color']) == 'string') {
            // el mismo color para todos
            colors = plot.data[traceID].x.map(item => default_color);
        }
        else {
            // un color para cada punto
            colors = marker['color'].map(item => item);
        }

        // trick: set pointID
        let pointID = pointIDs.reduce((a,b) => Math.min(a,b));  // get the smallest index from the set

        // debugger
        debugger;

        // original color of trace[pointID]
        let base_color = 'rgb(243, 186, 91)';       // amarillo por defecto; el codigo seria muucho mas simple si definimos un estilo/paleta de colores por defecto!
                                                    // en lugar de buscar los colores en el trace
        for (let j=0; j<colors.length; j++) {
            if (j != pointID && colors[j] != new_color) {
                base_color = colors[j];
                break;
            }
        }

        // debug
        debugger;

        // trick: change points
        for (let idx of pointIDs) {
            if (colors[idx] == new_color) {
                // go back to the original color
                colors[idx] = base_color;
            }
            else {
                // change its color
                colors[idx] = new_color;
            }
        }

        //colors[pointID] = "red";                                  // hightlight point
        //colors[pointID] = new_color;                                  // hightlight point

        // update style
        marker['color'] = colors; // no ocupa wrapper
        let update = {
            marker: marker
        };

        Plotly.restyle(plotName, update, traceIDs);
        //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces

        // we need to set a timeout to avoid a recursive call
        // when the click is done in a scatter3D
        // https://github.com/plotly/plotly.js/issues/1025
        //setTimeout(() => {
        //    Plotly.restyle(plotName, update, [traceID]);
        //}, 200);

        // debug
        console.log(`Update plot: ${plotName}, traceIDs: ${traceID}`);
    
}


// ---- add event handler ---

let plot_scatter_matrix = document.getElementById('scatter_matrix');

// click on points
plot_scatter_matrix.on('plotly_click', function(data){
    
    // check
    if (data === undefined || data === null) {
        console.log("Event On: Invalid event data");
        return 0;
    }

    // get more data
    let traceName = data.points[0].data.name; // asumiendo que solo hay un punto en points (ie que se hizo click en un solo punto)
    let pointID = data.points[0].pointNumber;

    console.log(`traceName: ${traceName}, pointID: ${pointID}`);

    // highlight the selected point in other plots
    highlightPointByName2("scatter_matrix", traceName, pointID, "red");

});


// lasso selection
plot_scatter_matrix.on('plotly_selected', function(data) {

    // check
    if (data === undefined || data === null) {
        console.log("Lasso: Invalid event data");
        return 0;
    }

    // check
    if (data.points.length === 0) {
        console.log("Lasso: 0 points selected");
        return 0;
    }

    // get more data
    let traceName = data.points[0].data.name;
    let pointIDs = data.points.map(point => point.pointNumber);

    console.log(`traceName: ${traceName}, pointIDs: ${pointIDs}`);

    // a bit slower
    //for (let pointID of pointIDs) {
    //    highlightPointByName2("scatter_matrix", traceName, pointID,"red");
    //}
    
    // faster
    highlightMultiplePointsByName2("scatter_matrix", traceName, pointIDs, "red");
});

// lasso selection
plot_scatter_matrix.on('plotly_deselect', function(data) {

    // check
    if (data === undefined || data === null) {
        console.log("Lasso deselect: Invalid event data");
        return 0;
    }

    // check
    if (data.points.length === 0) {
        console.log("Lasso deselect: 0 points selected");
        return 0;
    }

    // get more data
    let traceName = data.points[0].data.name;
    let pointIDs = data.points.map(point => point.pointNumber);

    console.log(`traceName: ${traceName}, pointIDs: ${pointIDs}`);

    // a bit slower
    //for (let pointID of pointIDs) {
    //    highlightPointByName2("scatter_matrix", traceName, pointID,"red");
    //}
    
    // faster
    //highlightMultiplePointsByName2("scatter_matrix", traceName, pointIDs, "red");
});