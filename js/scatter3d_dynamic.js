/* graph.js */
// This script contains our tiny library for creating PCP and scatter matrix

// @ts-check

// @todo: specify width and height of PCP
// @todo: specify width and height of scatter matrix
// @todo: probar con el dataset de DEBM2DK
// @todo: integrar el codigo de paco
// @todo: buscar evento para mover axes de PCP

// done @todo: mostrar los labels de los axes que se encuentran en la L del grid
// done @todo: definir colores del theme
// done @todo: agregar button 'Reset selection'
// done @todo: colocar 'obj' en los labels de los axes (mejor solo colocaremos el nombre de la columna del csv)
// done @todo: probar con mas de tres objs
// done @todo: force highlight
// done @todo: agregar color gris al panel del scatter matrix


// THEME ---------------------------------------------
const COLORSCALE = 'Blues';
const PCP_PLOTNAME = 'pcp';
const SCATTERMAT2D_PLOTNAME = 'scattermat2D';
const SCATTER3D_PLOTNAME = 'scatter3D';
const PALETTEVIZ_PLOTNAME = 'paletteviz';

// PATHS ---------------------------------------------

const URL_EXPORT_JSON = "http://127.0.0.1:8000/knee/default/export_json";   // controller: detault.py, function: export_json

// UTILS ---------------------------------------------

/*
Extract a colum from a dataset. The dataset has this format:

let rows = [
        {a: 0, b: 1, c: 2},
        {a: 1, b: 2, c: 3},
        {a: 2, b: 3, c: 4},
        {a: 3, b: 4, c: 5},
    ];
*/
/**
 * @param {any[]} rows
 * @param {string} key
 */
function unpack(rows, key) {
    return rows.map(row => row[key]);
}

/*
Repeat a value and return a list
*/
/**
 * @param {any} value
 * @param {number} times
 */
function repeat(value, times) {

  // check limit
  if (typeof(times) != 'number') {
    console.warn(`repeat function: invalid times value (${times})`);
    return [];
  }

  let list = [];
  for (let i=0; i<times; i++) {
      list.push(value);
  }
  return list;
}

/*
Transforms a dataset into a list of lists. The dataset has this format:

let rows = [
        {a: 0, b: 1, c: 2},
        {a: 1, b: 2, c: 3},
        {a: 2, b: 3, c: 4},
        {a: 3, b: 4, c: 5},
    ];

The new dataset has this format:

let data = [
  [0, 1, 2],
  [1, 2, 3],
  [2, 3, 4],
  [3, 4, 5]
]

In order to preserve the order of the columns, a cols list must be specified, for example:

let cols = ["a", "b", "c"]

*/
function transformRows(rows, cols) {

  let data = [];
  for (let i=0; i<rows.length; i++) {
    let newRow = [];
    for (let j=0; j<cols.length; j++) {
      newRow.push(rows[i][cols[j]]);
    }
    data.push(newRow);
  }

  return data;
}

/*
Returns the min value and max value of all objectives.
*/
function getBounds(rows) {

  let keys = Object.keys(rows[0]);
  let key = keys[0];
  let min = rows[0][key];
  let max = min;

  for (let i=0; i<rows.length; i++) {
    for (let key of keys) {
      let val = rows[i][key];
      if (val < min) {
        min = val;
      }
      if (val > max) {
        max = val;
      }
    }
  }

  return {min: min, max: max};

}

// PCP -----------------------------------------------

/*
Returns a trace for PCP
Check https://plotly.com/javascript/reference/parcoords/ for more info

Options for colorscale:
   Greys,YlGnBu,Greens,YlOrRd,Bluered,RdBu,Reds,Blues,Picnic,Rainbow,Portland,Jet,Hot,Blackbody,Earth,Electric,Viridis,Cividis

We create a trace from the data given in `rows`, for example:

    let rows = [
        {a: 0, b: 1, c: 2},
        {a: 1, b: 2, c: 3},
        {a: 2, b: 3, c: 4},
        {a: 3, b: 4, c: 5},
    ];
 
The color of each point is defined in line.color as a list:

    line: {
          color: [0,0,0,0],
          cmin: 0,
          cmax: 1,
          colorscale: 'Blues',
          reversescale: true
    }

We use a colorscale called 'Blue'. The color of each point must be defined in the range [0, 1],
where 0 is the minimum shade of blue and 1 is the maximum shade of blue. By default, we assign
zero to each point.

The trace contains `m` dimensions, one for each objective function. Note that `m == cols.length`.

*/
/**
 * @param {string} traceName
 * @param {any[]} rows
 * @param {any[]} cols
 * @param {number} min
 * @param {number} max
 */
function pcp_makeTrace(traceName, rows, cols, min, max) {

  // create dimensions
  let dimensions = cols.map(col => {

    return {
      label: col,
      values: unpack(rows, col),
      range: [min, max],                // @todo: define this
      //tickvals: [0, 1, 2, 3, 4, 5]  // @todo: define this
    }

  });

  let trace = {
    name: traceName,
    type: 'parcoords',
    line: {
      color: repeat(0, rows.length),       // default value 0: minimum shade of blue
      cmin: 0,
      cmax: 1,
      colorscale: COLORSCALE,
      reversescale: true
    },
    dimensions: dimensions
  };

  return trace;
}

/*
Highlights points in the PCP. The IDs of the points to be highlighted are specified
in pointIDs.
*/
/**
 * @param {number[]} pointIDs
 */
function pcp_highlightPoints(pointIDs) {
  
  let plot = document.getElementById(PCP_PLOTNAME);               // reference to the plot
  let traceID = 0;                                                // we suppose there is only one trace in the PCP
  let colors = plot.data[traceID].line.color.map(color => color); // trick: copy original colors (we assume this is a list, not a string) 

  // highlight here
  /*for (let id of pointIDs) {
    if (colors[id] == 1) {
      colors[id] = 0;   // original color 0: min shade of blue
    }
    else {
      colors[id] = 1;   // change color to 1: max shade of blue
    }
  }*/

  // force highlight
  for (let id of pointIDs) {
    colors[id] = 1;   // change color to 1: max shade of blue
  }

  // update style
  let update = {
    line: {
      color: colors,    // we highlight the points here!
      cmin: 0,
      cmax: 1,
      colorscale: COLORSCALE,
      reversescale: true
    }
  }

  Plotly.restyle(PCP_PLOTNAME, update, [traceID]);
}

/*
Changes the color of the points to the original color
*/
function pcp_resetPoints() {
  
  let plot = document.getElementById(PCP_PLOTNAME);               // reference to the plot
  let traceID = 0;                                                // we suppose there is only one trace in the PCP
  let colors = plot.data[traceID].line.color.map(color => 0);     // back to original color (0, ie the lowest shade of blue)

  // update style
  let update = {
    line: {
      color: colors,
      cmin: 0,
      cmax: 1,
      colorscale: COLORSCALE,
      reversescale: true
    }
  }

  Plotly.restyle(PCP_PLOTNAME, update, [traceID]);
}


/*
Creates a PCP plot
*/
/**
 * @param {any[]} rows
 * @param {any[]} cols
 */
function pcp_makePlot(rows, cols, min, max) {

  let trace = pcp_makeTrace('Serie', rows, cols, min, max);
  let data = [trace];
  Plotly.plot(PCP_PLOTNAME, data);

}

// SCATTER MATRIX ------------------------------------

/*
Returns a trace for scatter matrix 2D
Call this function like this:

  let trace_x1y1 = scattermat2D_makeTrace2D('Serie', rows, 'obj1', 'obj2', 'x1', 'y1');
*/
/**
 * @param {string} traceName
 * @param {any[]} rows
 * @param {string} xdata
 * @param {string} ydata
 * @param {string} xaxis
 * @param {string} yaxis
 */
function scattermat2D_makeTrace2D(traceName, rows, xdata, ydata, xaxis, yaxis) {
  
  let trace = {
    name: traceName,
    type: 'scatter',
    mode: 'markers',
    x: unpack(rows, xdata),
    y: unpack(rows, ydata),
    xaxis: xaxis,
    yaxis: yaxis,
    marker: {
      //color: MARKER_COLOR_DEFAULT,                     // not recommended: this specifies the same color for all points using a string
      color: repeat(MARKER_COLOR_DEFAULT, rows.length),  // recommended: we define a color for each point using a list rather than a single color (string)      size: 10,
      symbol: 'circle',
      opacity: 0.8,
      line: {
          color: repeat(LINE_COLOR, rows.length),
          width: 1
      }
    }
  };

  return trace;
}

/*
Creates a scatter matrix of shape (dims, dims), where `dims == cols.length`.
In order to keep the aspect-ratio of the subplot, we must define the lower bound
and upper bound. We use the same bounds for all the subplots in the grid.

The grid is constructed as follows (dims=3)
          __________________________________________________
        |                |                |                |
    a   | (i: 1, x1, y1) | (i: 2, x1, y1) | (i: 3, x1, y1) |
        |________________|________________|________________|
        |                |                |                |
    b   | (i: 4, x1, y1) | (i: 5, x1, y1) | (i: 6, x1, y1) |
        |________________|________________|________________|
        |                |                |                |
    c   | (i: 7, x1, y1) | (i: 8, x1, y1) | (i: 9, x1, y1) |
        |________________|________________|________________|

                 a                 b                c    

where i is the subplot number. The x label is defined as:

  let xlabel = cols[(i-1) % dims];

and the y label is:

  let ylabel = cols[Math.trunc((i-1)/dims)];

If you want to show only the labels on the left and the bottom of the grid (like an L shape), 
use this:

  if (!((i-1) % dims == 0)) {
    ylabel = "";
  }

  if (i <= (limit - dims)) {
    xlabel = "";
  }


*/
/**
 * @param {any[]} rows
 * @param {any[]} cols
 * @param {number} lbound
 * @param {number} ubound
 */
function scattermat2D_makePlot(rows, cols, lbound, ubound) {

  // dimensions
  let dims = cols.length;

  // define layout
  let layout = {};
  let limit = dims*dims;
  for (let i=1; i<=limit; i++) {

    let xlabel = cols[(i-1) % dims];
    let ylabel = cols[Math.trunc((i-1)/dims)];

    if (!((i-1) % dims == 0)) {
      ylabel = "";
    }

    if (i <= (limit - dims)) {
      xlabel = "";
    }

    let xkey = `xaxis${i}`;
    let ykey = `yaxis${i}`;
    layout[xkey] = { title: { text: xlabel }, range: [lbound, ubound] };   // @todo: change range
    layout[ykey] = { title: { text: ylabel }, range: [lbound, ubound] };   // @todo: change range
    
  }

  // define other properties
  layout['dragmode'] = 'lasso';
  layout['width'] = 1500; // 900
  layout['height'] = 1500; // 900
  layout['grid'] = {rows: dims, columns: dims, pattern: 'independent'};
  layout['showlegend'] = false;
  layout['plot_bgcolor'] = PLOT_BGCOLOR;

  // disable hover
  // https://stackoverflow.com/questions/35705885/disable-hover-in-plotly-js
  layout['hovermode'] = 'closest';    // options: "x", "y", "closest", false
    
  // create traces
  let data = [];
  let count = 1;              // note: we start at 1
  for (let ycol of cols) {
    for (let xcol of cols) {
      
      let xaxis = `x${count}`;
      let yaxis = `y${count}`;
      let trace_xy = scattermat2D_makeTrace2D('Serie', rows, xcol, ycol, xaxis, yaxis);

      data.push(trace_xy);    // save trace
      count = count + 1;      // next subplot
    }
  }

  Plotly.plot(SCATTERMAT2D_PLOTNAME, data, layout);
}

/*
Copies the data in the marker to avoid side-effects (mutations) when changing 
the color of the trace
*/
/**
 * @param {any} marker
 */
function scattermat2D_copyMarker(marker) {

  let marker_color = marker['color'].map(item => item);     // copy list of colors
  let line_color = marker.line['color'].map(item => item);  // copy list of colors

  let newMarker = {
      color: marker_color,
      size: marker['size'],
      symbol: marker['symbol'],
      opacity: marker['opacity'],
      line: {
          color: line_color,
          width: marker.line['width']
      }
  };

  return newMarker;
}

/*
 Returns the ID of the trace called traceName in plotName.
*/
/**
 * @param {string} traceName
 */
function scattermat2D_getTraceIDsByName(traceName) {

  let ids = [];
  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  
  // check if plot is valid
  if (plot === null) {
      console.warn(`Invalid plotName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: ${traceName})`);
      return ids;
  }

  // filter ids
  let traces = plot.data;
  for (let i=0; i<traces.length; i++) {
      if (traces[i].name == traceName) {
          ids.push(i);
      }
  }
  
  return ids;
}

/*
Highlight points in scatter matrix 2D
*/
/**
 * @param {number[]} pointIDs
 */
function scattermat2D_highlightPoints(pointIDs) {

  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  let traceIDs = scattermat2D_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

  // check if references are valid
  if (plot === null || traceIDs === null) {
      console.warn(`Invalid traceName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
      return 0;
  }

  // trick: inspect only the first traceID since their points are in the same order
  let traceID = traceIDs[0];

  // copy marker and original colors before changing colors
  let marker = scattermat2D_copyMarker(plot.data[traceID].marker);
  let colors = marker['color'].map(item => item);

  // trick: change points
  /*for (let idx of pointIDs) {
    if (colors[idx] == MARKER_COLOR_HIGHLIGHT) {
        colors[idx] = MARKER_COLOR_DEFAULT;     // go back to the original color
    }
    else {
        colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
    }
  }*/
  
  // force highlight
  for (let idx of pointIDs) {
    colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
  }

  // update style
  marker['color'] = colors;     // do not use marker['color'] = [colors]
  let update = {
      marker: marker
  };

  Plotly.restyle(SCATTERMAT2D_PLOTNAME, update, traceIDs);
}

/*
Changes the color of the points to the original color
*/
function scattermat2D_resetPoints() {

  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  let traceIDs = scattermat2D_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

  // check if references are valid
  if (plot === null || traceIDs === null) {
      console.warn(`Invalid traceName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
      return 0;
  }

  // trick: inspect only the first traceID since their points are in the same order
  let traceID = traceIDs[0];

  // copy marker and original colors before changing colors
  let marker = scattermat2D_copyMarker(plot.data[traceID].marker);
  let colors = marker['color'].map(item => MARKER_COLOR_DEFAULT);           // go back to original color

  // update style
  marker['color'] = colors;     // do not use marker['color'] = [colors]
  let update = {
      marker: marker
  };

  Plotly.restyle(SCATTERMAT2D_PLOTNAME, update, traceIDs);
}

// SCATTER 3D ------------------------------------

/*
Returns a trace for scatter matrix 2D
Call this function like this:

  let trace_x1y1 = scattermat2D_makeTrace2D('Serie', rows, 'obj1', 'obj2', 'x1', 'y1');
*/
/**
 * @param {string} traceName
 * @param {any[]} rows
 * @param {string} xdata
 * @param {string} ydata
 * @param {string} zdata
 * @param {string} xaxis
 * @param {string} yaxis
 * @param {string} zaxis
 */
function scatter3D_makeTrace3D(traceName, rows, xdata, ydata, zdata, xaxis, yaxis, zaxis) {
  
  let trace = {
    name: traceName,
    type: 'scatter3d',
    mode: 'markers',
    x: unpack(rows, xdata),
    y: unpack(rows, ydata),
    z: unpack(rows, zdata),
    xaxis: xaxis,
    yaxis: yaxis,
    zaxis: zaxis,
    hovertemplate: "Price: %{y:$.2f}<extra></extra>",    // https://plotly.com/javascript/reference/scatter3d/
    marker: {
      //color: MARKER_COLOR_DEFAULT,                     // not recommended: this specifies the same color for all points using a string
      color: repeat(MARKER_COLOR_DEFAULT, rows.length),  // recommended: we define a color for each point using a list rather than a single color (string)      size: 10,
      symbol: 'circle',
      opacity: 0.8,
      line: {
          color: repeat(LINE_COLOR, rows.length),
          width: 1
      }
    }
  };

  return trace;
}

/*
Creates a scatter matrix of shape (dims, dims), where `dims == cols.length`.
In order to keep the aspect-ratio of the subplot, we must define the lower bound
and upper bound. We use the same bounds for all the subplots in the grid.

The grid is constructed as follows (dims=3)
          __________________________________________________
        |                |                |                |
    a   | (i: 1, x1, y1) | (i: 2, x1, y1) | (i: 3, x1, y1) |
        |________________|________________|________________|
        |                |                |                |
    b   | (i: 4, x1, y1) | (i: 5, x1, y1) | (i: 6, x1, y1) |
        |________________|________________|________________|
        |                |                |                |
    c   | (i: 7, x1, y1) | (i: 8, x1, y1) | (i: 9, x1, y1) |
        |________________|________________|________________|

                 a                 b                c    

where i is the subplot number. The x label is defined as:

  let xlabel = cols[(i-1) % dims];

and the y label is:

  let ylabel = cols[Math.trunc((i-1)/dims)];

If you want to show only the labels on the left and the bottom of the grid (like an L shape), 
use this:

  if (!((i-1) % dims == 0)) {
    ylabel = "";
  }

  if (i <= (limit - dims)) {
    xlabel = "";
  }


*/
/**
 * @param {any[]} rows
 * @param {any[]} cols
 * @param {number} lbound
 * @param {number} ubound
 */
function scatter3D_makePlot(rows, cols, lbound, ubound) {

  debugger;

  // dimensions
  let dims = cols.length;

  let layout = {
    dragmode: 'lasso',
    hovermode: 'closest',
    width: 500,
    height: 500,
    showlegend: false,
    plot_bgcolor: PLOT_BGCOLOR,
    margin: { l: 0, r: 0, b: 0, t: 0 },
    scene: {
      xaxis: { title: cols[0], range: [lbound, ubound], showspikes: false },
      yaxis: { title: cols[1], range: [lbound, ubound], showspikes: false },
      zaxis: { title: cols[2], range: [lbound, ubound], showspikes: false },
    }
  };

  // create traces
  let trace = scatter3D_makeTrace3D('Serie', rows, cols[0], cols[1], cols[2], 'xaxis', 'yaxis', 'zaxis');
  let data = [trace];

  
  // -----

  // define layout
  /*let layout = {};
  let limit = dims*dims;
  for (let i=1; i<=limit; i++) {

    let xlabel = cols[(i-1) % dims];
    let ylabel = cols[Math.trunc((i-1)/dims)];

    if (!((i-1) % dims == 0)) {
      ylabel = "";
    }

    if (i <= (limit - dims)) {
      xlabel = "";
    }

    let xkey = `xaxis${i}`;
    let ykey = `yaxis${i}`;
    layout[xkey] = { title: { text: xlabel }, range: [lbound, ubound] };   // @todo: change range
    layout[ykey] = { title: { text: ylabel }, range: [lbound, ubound] };   // @todo: change range
    
  }

  // define other properties
  layout['dragmode'] = 'lasso';
  layout['width'] = 1500; // 900
  layout['height'] = 1500; // 900
  layout['grid'] = {rows: dims, columns: dims, pattern: 'independent'};
  layout['showlegend'] = false;
  layout['plot_bgcolor'] = PLOT_BGCOLOR;*/

  // disable hover
  // https://stackoverflow.com/questions/35705885/disable-hover-in-plotly-js
  //layout['hovermode'] = 'closest';    // options: "x", "y", "closest", false
    
  // create traces
  /*let data = [];
  let count = 1;              // note: we start at 1
  for (let ycol of cols) {
    for (let xcol of cols) {
      
      let xaxis = `x${count}`;
      let yaxis = `y${count}`;
      let trace_xy = scattermat2D_makeTrace2D('Serie', rows, xcol, ycol, xaxis, yaxis);

      data.push(trace_xy);    // save trace
      count = count + 1;      // next subplot
    }
  }*/

  Plotly.plot(SCATTER3D_PLOTNAME, data, layout);
}

/*
Copies the data in the marker to avoid side-effects (mutations) when changing 
the color of the trace
*/
/**
 * @param {any} marker
 */
function scatter3D_copyMarker(marker) {

  let marker_color = marker['color'].map(item => item);     // copy list of colors
  let line_color = marker.line['color'].map(item => item);  // copy list of colors

  let newMarker = {
      color: marker_color,
      size: marker['size'],
      symbol: marker['symbol'],
      opacity: marker['opacity'],
      line: {
          color: line_color,
          width: marker.line['width']
      }
  };

  return newMarker;
}

/*
 Returns the ID of the trace called traceName in plotName.
*/
/**
 * @param {string} traceName
 */
function scatter3D_getTraceIDsByName(traceName) {

  let ids = [];
  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  
  // check if plot is valid
  if (plot === null) {
      console.warn(`Invalid plotName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: ${traceName})`);
      return ids;
  }

  // filter ids
  let traces = plot.data;
  for (let i=0; i<traces.length; i++) {
      if (traces[i].name == traceName) {
          ids.push(i);
      }
  }
  
  return ids;
}

/*
Highlight points in scatter matrix 2D
*/
/**
 * @param {number[]} pointIDs
 */
function scatter3D_highlightPoints(pointIDs) {

  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  let traceIDs = scattermat2D_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

  // check if references are valid
  if (plot === null || traceIDs === null) {
      console.warn(`Invalid traceName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
      return 0;
  }

  // trick: inspect only the first traceID since their points are in the same order
  let traceID = traceIDs[0];

  // copy marker and original colors before changing colors
  let marker = scattermat2D_copyMarker(plot.data[traceID].marker);
  let colors = marker['color'].map(item => item);

  // trick: change points
  /*for (let idx of pointIDs) {
    if (colors[idx] == MARKER_COLOR_HIGHLIGHT) {
        colors[idx] = MARKER_COLOR_DEFAULT;     // go back to the original color
    }
    else {
        colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
    }
  }*/
  
  // force highlight
  for (let idx of pointIDs) {
    colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
  }

  // update style
  marker['color'] = colors;     // do not use marker['color'] = [colors]
  let update = {
      marker: marker
  };

  Plotly.restyle(SCATTERMAT2D_PLOTNAME, update, traceIDs);
}

/*
Changes the color of the points to the original color
*/
function scatter3D_resetPoints() {

  let plot = document.getElementById(SCATTERMAT2D_PLOTNAME);
  let traceIDs = scattermat2D_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

  // check if references are valid
  if (plot === null || traceIDs === null) {
      console.warn(`Invalid traceName (plotName: ${SCATTERMAT2D_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
      return 0;
  }

  // trick: inspect only the first traceID since their points are in the same order
  let traceID = traceIDs[0];

  // copy marker and original colors before changing colors
  let marker = scattermat2D_copyMarker(plot.data[traceID].marker);
  let colors = marker['color'].map(item => MARKER_COLOR_DEFAULT);           // go back to original color

  // update style
  marker['color'] = colors;     // do not use marker['color'] = [colors]
  let update = {
      marker: marker
  };

  Plotly.restyle(SCATTERMAT2D_PLOTNAME, update, traceIDs);
}

// PALETTEVIZ ----------------------------------------

/*
Copies the data in the marker to avoid side-effects (mutations) when changing 
the color of the trace
*/
/**
* @param {any} marker
*/
function paletteviz_copyMarker(marker) {

  let marker_color = marker['color'].map(item => item);     // copy list of colors
  let line_color = marker.line['color'].map(item => item);  // copy list of colors

  let newMarker = {
      color: marker_color,
      size: marker['size'],
      symbol: marker['symbol'],
      opacity: marker['opacity'],
      line: {
          color: line_color,
          width: marker.line['width']
      }
  };

  return newMarker;
}
/*
Returns the ID of the trace called traceName in plotName.
*/
/**
* @param {string} traceName
*/
function paletteviz_getTraceIDsByName(traceName) {

    let ids = [];
    let plot = document.getElementById(PALETTEVIZ_PLOTNAME);
    
    // check if plot is valid
    if (plot === null) {
        console.warn(`Invalid plotName (plotName: ${PALETTEVIZ_PLOTNAME}, traceName: ${traceName})`);
        return ids;
    }

    // filter ids
    let traces = plot.data;
    for (let i=0; i<traces.length; i++) {
        if (traces[i].name == traceName) {
            ids.push(i);
        }
    }
    
    return ids;
}

/*
Highlight points in paletteviz
*/
/**
* @param {number[]} pointIDs
*/
function paletteviz_highlightPoints(pointIDs) {

    let plot = document.getElementById(PALETTEVIZ_PLOTNAME);
    let traceIDs = paletteviz_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

    // check if references are valid
    if (plot === null || traceIDs === null) {
        console.warn(`Invalid traceName (plotName: ${PALETTEVIZ_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
        return 0;
    }

    // trick: inspect only the first traceID since their points are in the same order
    let traceID = traceIDs[0];

    // copy marker and original colors before changing colors
    let marker = paletteviz_copyMarker(plot.data[traceID].marker);
    let colors = marker['color'].map(item => item);

    // trick: change points
    /*for (let idx of pointIDs) {
    if (colors[idx] == MARKER_COLOR_HIGHLIGHT) {
        colors[idx] = MARKER_COLOR_DEFAULT;     // go back to the original color
    }
    else {
        colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
    }
    }*/
    
    // force highlight
    for (let idx of pointIDs) {
    colors[idx] = MARKER_COLOR_HIGHLIGHT;   // change color
    }

    // update style
    marker['color'] = colors;     // do not use marker['color'] = [colors]
    let update = {
        marker: marker
    };

    // we need to set a timeout to avoid a recursive call
    // when the click is done in a scatter3D
    // https://github.com/plotly/plotly.js/issues/1025
    setTimeout(() => {
      Plotly.restyle(PALETTEVIZ_PLOTNAME, update, traceIDs);
    }, 100);
}

/*
Changes the color of the points to the original color
*/
function paletteviz_resetPoints() {

    let plot = document.getElementById(PALETTEVIZ_PLOTNAME);
    let traceIDs = paletteviz_getTraceIDsByName('Serie');   // get all the IDs of the traces with name 'Serie', 
                                                            // this must be a list of dims*dims items, one trace for each subplot in the grid
                                                            // @todo: use SINGLE_TRACE_NAME rather than 'Serie'

    // check if references are valid
    if (plot === null || traceIDs === null) {
        console.warn(`Invalid traceName (plotName: ${PALETTEVIZ_PLOTNAME}, traceName: Serie, pointID: ${pointIDs})`);
        return 0;
    }

    // trick: inspect only the first traceID since their points are in the same order
    let traceID = traceIDs[0];

    // copy marker and original colors before changing colors
    let marker = paletteviz_copyMarker(plot.data[traceID].marker);
    let colors = marker['color'].map(item => MARKER_COLOR_DEFAULT);           // go back to original color

    // update style
    marker['color'] = colors;     // do not use marker['color'] = [colors]
    let update = {
        marker: marker
    };

    // we need to set a timeout to avoid a recursive call
    // when the click is done in a scatter3D
    // https://github.com/plotly/plotly.js/issues/1025
    setTimeout(() => {
      Plotly.restyle(PALETTEVIZ_PLOTNAME, update, traceIDs);
    }, 100);
}


// EVENT HANDLERS ------------------------------------

/*
Add event handlers to plots
Call this function from createPlots(), that is, after creating the plots
Otherwise, it will throw an error since you cannot assign an event handler
to an inexisting plot!
*/
function addEventHandlers() {

  let scattermat2D = document.getElementById(SCATTERMAT2D_PLOTNAME);

  // click on a single point
  scattermat2D.on('plotly_click', function(data){
      
      // check
      if (data === undefined || data === null) {
          console.log("scattermat2d event on: Invalid event data");
          return 0;
      }

      // get more data
      let traceName = data.points[0].data.name; 
      let pointID = data.points[0].pointNumber; // here, we assume the user clicked on a single point (ie, data.points[0])

      console.log(`traceName: ${traceName}, pointID: ${pointID}`);

      // highlight the selected point in other plots
      let pointIDs = [pointID];
      scattermat2D_highlightPoints(pointIDs);
      pcp_highlightPoints(pointIDs);
      paletteviz_highlightPoints(pointIDs);
  });

  // lasso selection
  scattermat2D.on('plotly_selected', function(data) {

    // check
    if (data === undefined || data === null) {
        console.log("Lasso: Invalid event data");
        return 0;
    }

    // check
    if (data.points.length === 0) {
        console.log("Lasso: 0 points selected");
        return 0;
    }

    // get more data
    let traceName = data.points[0].data.name;                   // here, we assume the user clicked on a single point (ie, data.points[0])
    let pointIDs = data.points.map(point => point.pointNumber); // get the ID of the points in the selection

    console.log(`traceName: ${traceName}, pointIDs: ${pointIDs}`);

    // highlight the selected point in other plots
    scattermat2D_highlightPoints(pointIDs);
    pcp_highlightPoints(pointIDs);
    paletteviz_highlightPoints(pointIDs);
  });

  let paletteviz = document.getElementById(PALETTEVIZ_PLOTNAME);

  // click on a single point
  paletteviz.on('plotly_click', function(data){
      
      // check
      if (data === undefined || data === null) {
          console.log("scattermat2d event on: Invalid event data");
          return 0;
      }

      // get more data
      let traceName = data.points[0].data.name; 
      let pointID = data.points[0].pointNumber; // here, we assume the user clicked on a single point (ie, data.points[0])

      console.log(`traceName: ${traceName}, pointID: ${pointID}`);

      // highlight the selected point in other plots
      let pointIDs = [pointID];
      scattermat2D_highlightPoints(pointIDs);
      pcp_highlightPoints(pointIDs);
      paletteviz_highlightPoints(pointIDs);
  });

}

// MAIN ----------------------------------------------

/*
Creates the PCP and scatter matrix plots.
Call this function after the user selects an MOP in the page.
*/
function createPlots(rows, cols) {

  let bounds = getBounds(rows);
  let min = bounds['min'];
  let max = bounds['max'];

  // classic plots
  //pcp_makePlot(rows, cols, min, max);
  scattermat2D_makePlot(rows, cols, min, max);
  scatter3D_makePlot(rows, cols, min, max);

  // paletteviz
  //let data = transformRows(rows, cols);
  //PaletteViz(data, 3);

  //addEventHandlers();
}


/*
This function do this:

  requestPlots() -> Make a POST request to enable the plotting area in the page
  createPlots()  -> Creates the scatter matrix and PCP, and add event handlers

*/
function requestPlots(mop_name, m_objs) {

  // @todo: replace this block with a POST request
  
  let params = `mop_name=${mop_name}&m_objs=${m_objs}`;

  // send get request
  let xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      // parse data
      let rows = JSON.parse(this.responseText);
      let cols = Object.keys(rows[0]);

      console.log(rows);

      // create plots
      createPlots(rows, cols);
    }
  };

  console.log("sending...");
  console.log(params);

  // https://stackoverflow.com/questions/9713058/send-post-data-using-xmlhttprequest
  xmlhttp.open("POST", URL_EXPORT_JSON, true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  
  // https://stackoverflow.com/questions/48263526/javascript-set-header-access-control-allow-origin
  xmlhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
  xmlhttp.setRequestHeader("Access-Control-Allow-Headers", "Content-Type");
  xmlhttp.setRequestHeader("Access-Control-Allow-Methods", "POST");
  xmlhttp.send(params);
}