/* scatter_highlight.js */
// @ts-check

// --- utils ---

function unpack(rows, key) {
    return rows.map(row => row[key]);
}

// --- get data ---

let rows = [
    {x: 0, a: 0, b: 1, c: 2},
    {x: 1, a: 1, b: 2, c: 3},
    {x: 2, a: 2, b: 3, c: 4},
    {x: 3, a: 3, b: 4, c: 5},
];

// --- plot ---

let trace_a = {
    x: unpack(rows, 'x'),
    y: unpack(rows, 'a'),
    name: 'Serie A',
    type: 'scatter',
    mode: 'lines+markers',
    marker: {
        color: 'rgb(243, 186, 91)',
        size: 10,
        symbol: 'circle',
        opacity: 0.8,
        line: {
            color: 'rgb(120, 120, 120)',
            width: 1
        }
    },
    line: {
        color: 'rgb(243, 186, 91)',
        width: 2
    }
};

let trace_b = {
    x: unpack(rows, 'x'),
    y: unpack(rows, 'b'),
    name: 'Serie B',
    type: 'scatter',
    mode: 'lines+markers',
    marker: {
        color: 'rgb(139, 167, 177)',
        size: 10,
        symbol: 'circle',
        opacity: 0.8,
        line: {
            color: 'rgb(120, 120, 120)',
            width: 1
        }
    },
    line: {
        color: 'rgb(139, 167, 177)',
        width: 2
    }
};

let trace_c = {
    x: unpack(rows, 'x'),
    y: unpack(rows, 'c'),
    name: 'Serie C',
    type: 'scatter',
    mode: 'lines+markers',
    marker: {
        color: 'rgb(139, 177, 161)',
        size: 10,
        symbol: 'circle',
        opacity: 0.8,
        line: {
            color: 'rgb(120, 120, 120)',
            width: 1
        }
    },
    line: {
        color: 'rgb(139, 177, 161)',
        width: 2
    }
};

let layout = {
    width: 600,
    height: 400
};

Plotly.plot('scatter_a', [trace_a, trace_b], layout);
Plotly.plot('scatter_b', [trace_a, trace_c], layout);

// ----

// debug
let scatter_a = document.getElementById('scatter_a');

// show points
let trace_id = 0;
console.log(scatter_a.data[trace_id].x);  // componente x
console.log(scatter_a.data[trace_id].y);  // componente y


/*

let colors = rows.map(item => null);
colors[3] = "green";

let update = {
    //'marker.color': [colors]      // we need to wrap colors with [], see https://plotly.com/javascript/plotlyjs-function-reference/#plotlyrestyle
    //'marker.color': [['red', 'blue', 'green']]  // funciona
    //'marker.color': [['undefined', 'blue', 'green']]  // no funciona
    //'marker.color': [[undefined, null, 'green']]  // no funciona
    'marker.color': [undefined, 'green']  // solo funciona a nivel de trace: el trace 0 es undefined (no cambia), y el trace 1 sera greeen
};

Plotly.restyle(scatter_a, update);*/

/*

// ----- function -----
let plotName = 'scatter_a';
let traceID = 0;
let pointID = 3;
let plot = document.getElementById(plotName);

// prev style
let marker = plot.data[traceID].marker;
let default_color = marker['color'];

let colors = plot.data[traceID].x.map(item => default_color);    // default color for all points
colors[pointID] = "rgb(0,0,0)";                                  // hightlight point


// update style
marker['color'] = colors; // no ocupa wrapper
let update = {
    marker: marker
};

Plotly.restyle(plotName, update, [traceID]);
//Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces

*/























// --- utils ---
function highlightPoint(plotName, traceID, pointID) {


    let plot = document.getElementById(plotName);

    // prev style
    let marker = plot.data[traceID].marker;
    let default_color = marker['color'];

    // aqui hay una condicion
    // si marker['color'] es un string, eg marker['color'] = 'rgb(0,0,0)'
    // entonces todo el trace es de ese color
    // si marker['color'] es una lista, eg marker['color'] = ['red', 'blue', ..., 'green']
    // entonces, cada punto del trace tiene un color asignado
    // debemos manejar ambos casos

    debugger;

    let colors;
    if (typeof(marker['color']) == 'string') {
        // el mismo color para todos
        colors = plot.data[traceID].x.map(item => default_color);
    }
    else {
        // un color para cada punto
        colors = marker['color'].map(item => item);
    }

    colors[pointID] = "red";                                  // hightlight point


    // update style
    marker['color'] = colors; // no ocupa wrapper
    let update = {
        marker: marker
    };

    Plotly.restyle(plotName, update, [traceID]);
    //Plotly.restyle(plotName, update);     // si no se especifica [traceID], se aplicara el mismo estilo a todos los traces



}