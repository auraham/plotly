// @ts-check

function unpack(rows, key) {
  return rows.map(row => row[key]);
}

// --- get data ---

let rows = [
  {x: 0, a: 0, b: 1, c: 2},
  {x: 1, a: 1, b: 2, c: 3},
  {x: 2, a: 2, b: 3, c: 4},
  {x: 3, a: 3, b: 4, c: 5},
];

let trace = {
  type: 'parcoords',
  // https://plotly.com/javascript/reference/parcoords/
  // coloscale: Greys,YlGnBu,Greens,YlOrRd,Bluered,RdBu,Reds,Blues,Picnic,Rainbow,Portland,Jet,Hot,Blackbody,Earth,Electric,Viridis,Cividis.
  line: {
    //color: [1,0.5,0,1],   // 1: blue shade, 0.5: mid shade, 0: gray shade
    color: [0,0,0,0],       // todos los puntos son grises por defecto
    cmin: 0,
    cmax: 1,
    colorscale: 'Blues',
    reversescale: true
  },
  
  dimensions: [{
    range: [0, 5],
    label: 'a',
    values: unpack(rows, 'a'),
    tickvals: [0, 1, 2, 3, 4, 5]
  }, {    
    range: [0, 5],
    label: 'b',
    values: unpack(rows, 'b'),
    tickvals: [0, 1, 2, 3, 4, 5]
  }, {
    range: [0, 5],
    label: 'c',
    values: unpack(rows, 'c'),
    tickvals: [0, 1, 2, 3, 4, 5]
    //tickvals: [1,2,4,5],
    //ticktext: ['text 1','text 2','text 4','text 5']
  }]
};

let data = [trace]

let layout = {
  hovermode: 'closest'
};

Plotly.plot('pcp', data, layout);

// ---- update style ----

/*
let plot_pcp = document.getElementById('pcp');

let traceID = 0;
let update = {
  line: {
    color: [0,0,0,1],       // resaltamos el 3-th punto (zero based)
    cmin: 0,
    cmax: 1,
    colorscale: 'Blues',
    reversescale: true
  },
  
}
Plotly.restyle('pcp', update, [traceID]);

debugger;
*/