/* colorscheme.js */
// This script contains the color scheme employed for the plots 
// created with graph.js and paletteviz.js

const MARKER_COLOR_DEFAULT = 'rgb(218, 218, 218)';
const MARKER_COLOR_HIGHLIGHT = 'rgb(53, 56, 178)';
const LINE_COLOR = 'rgb(120, 120, 120)';
const PLOT_BGCOLOR = 'rgb(245,245,255)';