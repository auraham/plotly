# dashboard.py

def index():
    """This function returns a form and a boolean variable (enable_plot_area):
        If the form is accepted, then enable_plot_area = True
        Otherwise, it is False.
    We then use enable_plot_area in the view (dashboard/index.html) for creating the plots.
    """
    # return value
    enable_plot_area = False

    # create form
    form = SQLFORM.factory()
    mop_names = ['DEBMDK']
    fields = [
        Field('mop_name', 'string', label='Problem', requires=IS_IN_SET(mop_names, zero='Choose a problem')),
        Field('m_objs', 'integer', label='# of objectives', requires=IS_IN_SET([3, 5, 8, 10], zero='Choose a value')),
    ]
    form = SQLFORM.factory(*fields)

    # validate form
    if form.process().accepted:
        response.flash = ""
        enable_plot_area = True

    elif form.errors:
        response.flash = "Invalid data"
    
    return dict(form=form, enable_plot_area=enable_plot_area)


def export_json():
    # https://stackoverflow.com/questions/40430152/return-a-list-as-json-from-web2py
    # export json
    data = [
        {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4},
        {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5},
        {"a": 2, "b": 3, "c": 4, "d": 5, "e": 6},
        {"a": 3, "b": 4, "c": 5, "d": 6, "e": 7}
        ]

    return response.json(data)


def test():
    response.title = "Hey!"
    return dict()