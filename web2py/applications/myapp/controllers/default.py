# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----
def index():
    #response.flash = T("Hello World")
    #return dict(message=T('Welcome to web2py!'))

    #return "Hello from myapp"
    #return dict(message="Hello from myapp")


    # counter
    if not session.counter:
        session.counter = 1
    else:
        session.counter += 1
    return dict(message="Hello from myapp", counter=session.counter)

def first():
    """
    if request.vars.visitor_name:
        # guarda el nombre en la sesion
        session.visitor_name = request.vars.visitor_name
        redirect(URL('second'))
    return dict()
    """

    """
    form = FORM(
            INPUT(_name="visitor_name", requires=IS_NOT_EMPTY()),
            INPUT(_type="submit"))

    if form.process().accepted:
        session.visitor_name = form.vars.visitor_name
        redirect(URL('second'))

    return dict(form=form)
    """

    # this way is better!
    form = SQLFORM.factory(Field("visitor_name",
                                 label="What is your name?",
                                 requires=IS_NOT_EMPTY()
                                ))
    if form.process().accepted:
        session.visitor_name = form.vars.visitor_name
        redirect(URL('second'))
    
    return dict(form=form)

def second():
    return dict()


def display_form():
    form = FORM('Your name:',
                INPUT(_name='name', requires=IS_NOT_EMPTY()),
                INPUT(_type='submit'))

    course_list = ['a', 'b', 'c']
    # Field(fieldname, type, label, requires)
    form = SQLFORM.factory(Field('courses', 'integer',
                            label='Courses',
                            requires=IS_IN_SET(course_list, zero='Choose course')
    ))

    if form.accepts(request, session):      # all the work is done here
        response.flash = "form accepted"

        # debug
        # form.vars <Storage {'name': 'aura'}>
        print("form.vars", form.vars)

    elif form.errors:
        response.flash = "form has errors"

        # debug
        # form.errors <Storage {'name': 'Enter a value'}>
        print("form.errors", form.errors)

    else:
        response.flash = "please fill the form"
        
        # debug
        # response.flash please fill the form
        print("response.flash", response.flash)

    # debug
    print("request.vars", request.vars)


    return dict(form=form)


def display_form_mop():

    form = SQLFORM.factory()
    mop_names = ['deb2mdk']
    fields = [
        Field('mop_name', 'string', label='Problem', requires=IS_IN_SET(mop_names, zero='Choose problem')),
        Field('m_objs', 'integer', label='# of objectives', requires=IS_IN_SET([3, 5, 8, 10], zero='Choose value')),
    ]
    form = SQLFORM.factory(*fields)

    if form.process().accepted:
        #response.flash = "Success!!!"
        #response.flash = ""
        True
    elif form.errors:
        response.flash = "There are errors"
    #else:
    #    response.flash = "Fill the form"

    return dict(form=form)


def export_json():
    # https://stackoverflow.com/questions/40430152/return-a-list-as-json-from-web2py
    # export json
    data = [
        {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4},
        {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5},
        {"a": 2, "b": 3, "c": 4, "d": 5, "e": 6},
        {"a": 3, "b": 4, "c": 5, "d": 6, "e": 7}
        ]

    return response.json(data)



# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Smart Grid (example) -----
@auth.requires_membership('admin') # can only be accessed by members of admin groupd
def grid():
    response.view = 'generic.html' # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----
def wiki():
    auth.wikimenu() # add the wiki to the menu
    return auth.wiki() 

# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
